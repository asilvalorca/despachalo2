'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TipoEstado extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  TipoEstado.init({
    id_tipo_estado: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    tipoestado: DataTypes.STRING,
    estado: {
        type: DataTypes.INTEGER,
        defaultValue: 1
    }
    
  }, {
    underscored: true,
    sequelize,
    modelName: 'TipoEstado',
  });
  return TipoEstado;
};