'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Perfil extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Perfil.init({
    id_perfil: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    perfil: DataTypes.STRING,
    estado: {
        type: DataTypes.INTEGER,
        defaultValue: 1
    }

  }, {
    underscored: true,
    sequelize,
    modelName: 'Perfil',
  });
  return Perfil;
};