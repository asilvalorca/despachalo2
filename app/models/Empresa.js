'use strict';
const { Model} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Empresa extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
   
      Empresa.belongsTo(models.Comuna, {as: 'Comuna', foreignKey: 'id_comuna'});
    }
  };
  Empresa.init({
    id_empresa: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: DataTypes.STRING,
    rut: DataTypes.STRING,
    giro:  DataTypes.STRING,
    direccion:  DataTypes.STRING,
    estado: {
        type: DataTypes.INTEGER,
        defaultValue: 1
    }
}, {
  underscored: true,
    sequelize,
    modelName: 'Empresa',
  });
  return Empresa;
};