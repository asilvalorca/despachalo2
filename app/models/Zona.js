'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Zona extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Zona.belongsToMany(models.Comuna, {as: 'Comunas', through: "zona_comuna" });
    }
  };
  Zona.init({
    id_zona: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    zona: DataTypes.STRING,
    estado: {
        type: DataTypes.INTEGER,
        defaultValue: 1
    }
  }, {
    underscored: true,
    sequelize,
    modelName: 'Zona',
  });
  return Zona;
};