'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Pago extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
        Pago.belongsTo(models.Envio, {as: 'Envio', foreignKey: 'id_envio'});
        Pago.belongsTo(models.Empresa, {as: 'Empresa', foreignKey: 'id_empresa'});
        Pago.belongsTo(models.EstadoPago, {as: 'EstadoPago', foreignKey: 'id_estado_pago'});
    }
  };
  Pago.init({
    id_pago: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
   
    monto:  DataTypes.BIGINT,
 
  }, {
    underscored: true,
    sequelize,
    modelName: 'Pago',
  });
  return Pago;
};