'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class EnvioDetalle extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      EnvioDetalle.belongsTo(models.Envio, {as: 'Envio', foreignKey: 'id_envio'});
      EnvioDetalle.belongsTo(models.TipoEstado, {as: 'TipoEstado', foreignKey: 'id_tipo_estado'});
      EnvioDetalle.belongsTo(models.Usuario, {as: 'Usuario', foreignKey: 'id_usuario'});
    }
  };
  EnvioDetalle.init({
    id_envio_detalle: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    comentario: DataTypes.STRING,
    
    

  }, {
    underscored: true,
    sequelize,
    modelName: 'EnvioDetalle',
  });
  return EnvioDetalle;
};