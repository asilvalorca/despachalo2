'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Usuario extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Usuario.belongsToMany(models.Zona, { through: "usuario_zona" }); 
      Usuario.belongsTo(models.Perfil, {as: 'Perfil', foreignKey: 'id_perfil'});
      Usuario.belongsTo(models.Empresa, {as: 'Empresa', foreignKey: 'id_empresa'});
    }
  };
  Usuario.init({
    id_usuario: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    correo:  DataTypes.STRING,
    nombre:  DataTypes.STRING,
    apellido: DataTypes.STRING,
    estado: {
        type: DataTypes.INTEGER,
        defaultValue: 1
    }
  }, {
    underscored: true,
    sequelize,
    modelName: 'Usuario',
  });
  return Usuario;
};