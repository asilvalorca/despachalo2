'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class DetalleCorreo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      DetalleCorreo.belongsTo(models.Envio, {as: 'Envio', foreignKey: 'id_envio'});
      DetalleCorreo.belongsTo(models.TipoEstado, {as: 'TipoEstado', foreignKey: 'id_tipo_estado'});
      DetalleCorreo.belongsTo(models.Usuario, {as: 'Usuario', foreignKey: 'id_usuario'});
    }
  };
  DetalleCorreo.init({
    id_envio_detalle: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    comentario: DataTypes.STRING,
    correo: DataTypes.STRING,
    
    

  }, {
    underscored: true,
    sequelize,
    modelName: 'DetalleCorreo',
  });
  return DetalleCorreo;
};