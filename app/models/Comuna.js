'use strict';
const {  Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Comuna extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Comuna.belongsToMany(models.Zona, {as: 'Zonas', through: "zona_comuna" });
    }
  };
  Comuna.init({
    id_comuna: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    comuna: DataTypes.STRING,
    estado: {
          type: DataTypes.INTEGER,
          defaultValue: 1
    },
    precio:
    {
          type: DataTypes.INTEGER,
          defaultValue: 0
    }
   
  }, {
    underscored: true,
    sequelize,
    modelName: 'Comuna',
  });
  return Comuna;
};