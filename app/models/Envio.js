'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Envio extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Envio.hasMany(models.EnvioDetalle, {as: 'EnvioDetalle', foreignKey: 'id_envio'});
      Envio.belongsTo(models.Empresa, {as: 'Empresa', foreignKey: 'id_empresa'});
      Envio.belongsTo(models.Usuario, {as: 'Usuario', foreignKey: 'id_usuario'});
      Envio.belongsTo(models.Usuario, {as: 'UsuarioConductor', foreignKey: 'id_conductor'});
      Envio.belongsTo(models.Comuna, {as: 'Comuna', foreignKey: 'id_comuna'});
      Envio.belongsTo(models.TipoEstado, {as: 'TipoEstado', foreignKey: 'id_tipo_estado'});
    }
  };
  Envio.init({
    id_envio: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
   
    monto:  DataTypes.INTEGER,
    nombre_destinatario: DataTypes.STRING,
    rut: DataTypes.STRING,
    telefono: DataTypes.INTEGER,
    direccion: DataTypes.STRING,
    numero: DataTypes.INTEGER,
    correo: DataTypes.STRING,
    comentario: DataTypes.TEXT,
    fecha_compra: DataTypes.DATEONLY,
    fecha_entrega: DataTypes.DATEONLY,
  }, {
    underscored: true,
    sequelize,
    modelName: 'Envio',
  });
  return Envio;
};