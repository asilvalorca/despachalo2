import { Usuario, Perfil, Zona, Empresa } from "../models/index";
import { check, validationResult } from "express-validator";
import { Op } from "sequelize";
import bcrypt  from 'bcrypt';
import jwt from "jsonwebtoken";
import {sequelize} from "../models/index";
import {verificarAuth, verificarAdministrador}  from "../../routes/middlewares/autenticacion";

const saltRounds = 10;

module.exports = {

    async modificar(req, res){  
        //console.log(sequelize);
        
        console.log(results);
        console.log(metadata);

    },
    async crear(req, res){   

        
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(422).json({ errores: errors.array() });
        }
        const body = {
            username: req.body.username,
            id_perfil: req.body.id_perfil,
            nombre: req.body.nombre,
            correo: req.body.correo,
            apellido: req.body.apellido,  
            id_empresa: req.body.id_empresa!=""? req.body.id_empresa : null,  
          
        };
        
        body.password = bcrypt.hashSync(req.body.password, saltRounds);
        
        const buscarUsuario = await Usuario.findAll({
            where: {
                [Op.or]: [
                    { username: req.body.username },
                    { correo: req.body.correo }
                  ]
            }
          });
          if(!buscarUsuario){
            return  res.status(400).json({mensaje: "Usuario o contraseña incorrecta", estado: 0, errores: ""});   
        }
          if(buscarUsuario.length > 0){
           return res.status(403).json({mensaje: "El usuario ya existe", estado: 0, errores: ""});
           
          }
        console.log(req.body.zonas);
        try{
            
            const usuarioDB = await Usuario.create(body);
           
            if(req.body.zonas){
                console.log("paso primera parte");
                const zona = req.body.zonas;
                console.log(typeof zona);
                const zonas = zona.split(",");
                console.log("si hay zonas");
                if(zonas.length > 0){
                    const zonaArray = [];
                    console.log("se hizo split");
                    let cont = 0;
                    for (let elementoZona of zonas) {  
                        cont++;
                        let zonaDB = await Zona.findOne({ where: { id_zona: elementoZona } });
                        if(zonaDB){
                            zonaArray.push(elementoZona);
                        }   
                       
                    }
                    
                   await usuarioDB.setZonas(zonaArray).then(sc=>{
                    console.log(sc);
                    });

                }
                

               
            }
            //res.status(200).json(usuarioDB);
            res.status(200).json({mensaje: "Usuario guardado con exito", estado: 1, errores: ""});
        }catch (error){
            return res.status(500).json({
                mensaje: "ocurrio un error",
                error,
                estado: 0
            })
        }
    },

    async listarUsuarios(req, res){   
       
        const usuarioDB = await Usuario.findAll({ include:[{
            model: Perfil,
            as: "Perfil",
            attributes: ['perfil']
            },
            {
                model: Empresa,
                as: "Empresa",
                attributes: ['nombre'],
                required:false
            },{
                model: Zona,
                attributes: ['zona'],
                required:false
            }]
        });
    
        return res.status(200).json(usuarioDB);
    },

    async listarUsuariosActivos(req, res){   
        const usuarioDB = await Usuario.findAll({ include:[{
            model: Perfil,
            as: "Perfil",
            attributes: ['perfil']
            },
            {
                model: Empresa,
                as: "Empresa",
                attributes: ['nombre'],
                required:false
            },{
                model: Zona,
                attributes: ['zona'],
                required:false
            }]
        });
        
        return res.status(200).json(usuarioDB);
    },
    async listarConductores(req, res){   
        const usuarioDB = await Usuario.findAll({  where: {
            id_perfil: 2
        },
        include:[{
            model: Perfil,
            as: "Perfil",
            attributes: ['perfil']
            },
            {
                model: Empresa,
                as: "Empresa",
                attributes: ['nombre'],
                required:false
            },{
                model: Zona,
                attributes: ['zona'],
                required:false
            }]
        });
        
        return res.status(200).json(usuarioDB);
    },

    async actualizar(req, res){   
        const _id = req.params.id;
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(422).json({ errores: errors.array() });
        }

        const buscarUsuario = await Usuario.findAll({
            where: {
                [Op.or]: [
                    { username: req.body.username },
                    { correo: req.body.correo }
                ]
            }
        });
        if(!buscarUsuario){
            return  res.status(400).json({mensaje: "usuario no existe", estado: 0, errores: ""});   
        }
        if(buscarUsuario.length > 0){
            const id_usuario = buscarUsuario[0].dataValues.id_usuario;
            if(id_usuario != _id){
                return res.status(200).json({mensaje: "El usuario ya existe", estado: 0, errores: ""});
            
            }
        }

        const body = {
            username: req.body.username,
            id_perfil: req.body.id_perfil,
            nombre: req.body.nombre,
            correo: req.body.correo,
            apellido: req.body.apellido,    
            estado: req.body.estado,    
        
        };

        if(body.password){
            body.password = bcrypt.hashSync(req.body.password, saltRounds);
        }
    
        
        
        try{
            const usuarioDB = await Usuario.update(body,{
                where: {
                    id_usuario: _id
                }
            });
            if(!usuarioDB){
               
                return res.status(500).json({mensaje: "Id no encontrado", estado: 0, errores: ""});
            }else{

                const [results, metadata] = await sequelize.query("DELETE FROM `usuario_zona` WHERE `usuario_id_usuario`='"+_id+"'");
                if(req.body.zonas){
                    console.log("paso primera parte");
                    const zona = req.body.zonas;
                    console.log(typeof zona);
                    const zonas = zona.split(",");
                    console.log("si hay zonas");
                    if(zonas.length > 0){
                        const zonaArray = [];
                        console.log("se hizo split");
                        let cont = 0;
                        for (let elementoZona of zonas) {  
                            cont++;
                            let zonaDB = await Zona.findOne({ where: { id_zona: elementoZona } });
                            if(zonaDB){
                                zonaArray.push(elementoZona);
                                const [results, metadata] = await sequelize.query("INSERT INTO `usuario_zona` (`created_at`, `updated_at`, `usuario_id_usuario`, `zona_id_zona`) VALUES (NOW(), NOW(), '"+_id+"', '"+elementoZona+"')");
                            }   
                           
                        }
                        
                       /*await buscarUsuario.setZonas(zonaArray).then(sc=>{
                        console.log(sc);
                        });*/
    
                    }
                    
    
                   
                }
                return res.status(200).json({mensaje: "Usuario actualizado con exito", estado: 1, errores: ""});
            }
            
        
        }catch (error){
            return res.status(400).json({
                mensaje: "ocurrio un error",
                error
            })
        }
    },
    async login(req, res){   
        
        let condicion =[];
        let cont = 0;
        if( req.body.username !='' && req.body.username!==undefined){
            condicion.push( { username: req.body.username });
        }else{
            cont++;
        }
        if(req.body.correo !='' && req.body.correo!==undefined){
            condicion.push({ correo: req.body.correo });
        }else{
            cont++;
        }
        if(cont == 2){
            return  res.status(400).json({mensaje: "Debe enviar usuario o correo", estado: 0, errores: ""});  
        }
        
        const buscarUsuario = await Usuario.findOne({

            attributes: ['id_empresa','id_usuario', 'id_perfil', 'correo', 'nombre', 'username','apellido', "password"],
            where: {
                [Op.or]: condicion,
                
                
            }, include:[
                {
                    model: Empresa,
                    as: "Empresa",
                    attributes: ['nombre'],
                    required:false
                }]
        });
        if(!buscarUsuario){
            return  res.status(400).json({mensaje: "Usuario o contraseña incorrecta", estado: 0, errores: ""});   
        }
        if(buscarUsuario.length == 0){
            return  res.status(400).json({mensaje: "Usuario o contraseña incorrecta", estado: 0, errores: ""});   
        }
        console.log( req.body.password);
        console.log( buscarUsuario.password);
        if(!bcrypt.compareSync( req.body.password, buscarUsuario.password)){
            return  res.status(400).json({mensaje: "Usuario o contraseña incorrecta ", estado: 0, errores: ""});   
        }
        
        buscarUsuario.password = "";
        const token =  jwt.sign({
            data:buscarUsuario
        },'secret',{expiresIn: 60 * 60 * 24 * 30});

        return res.json({buscarUsuario, token});
    },
}