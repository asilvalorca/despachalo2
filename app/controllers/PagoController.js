import { Envio, Usuario, Comuna, EnvioDetalle, TipoEstado, Empresa, Pago, EstadoPago  } from "../models/index";
import { check, validationResult } from "express-validator";
import { Op } from "sequelize";
import xlsx from "xlsx"; 
import nodemailer from "nodemailer";

module.exports = {
   
  
    async listarPagosPendientes(req, res){   
        const _id = req.params.id;
        const pagoDB = await Pago.findAll({ where: {
            [Op.and]: [
                { id_empresa: _id },
                { id_estado_pago: 1 }
              ]
            },
            include:[{
                model: Empresa,
                as: "Empresa",
                attributes: ['nombre'],
                required:false
            },{
                model: Envio,
                as: "Envio",
                required:false
            },{
                model: EstadoPago,
                as: "EstadoPago",
                attributes: ['nombre'],
                required:false
            },
            ]
        });
    
        return res.status(200).json(pagoDB);
    },
    async listarPagos(req, res){   
        const _id = req.params.id;
        const pagoDB = await Pago.findAll({ where: {
                id_empresa: _id 
        },include:[{
                model: Empresa,
                as: "Empresa",
                attributes: ['nombre'],
                required:false
            },{
                model: Envio,
                as: "Envio",
                required:false
            },{
                model: EstadoPago,
                as: "EstadoPago",
                attributes: ['nombre'],
                required:false
            },
            ]
        });
    
        return res.status(200).json(pagoDB);
    },
    async listarTodosPagosPendientes(req, res){   
        const _id = req.params.id;
        const pagoDB = await Pago.findAll({ where: {
            [Op.and]: [
              
                { id_estado_pago: 1 }
              ]
            },
            include:[{
                model: Empresa,
                as: "Empresa",
                attributes: ['nombre'],
                required:false
            },{
                model: Envio,
                as: "Envio",
                required:false
            },{
                model: EstadoPago,
                as: "EstadoPago",
                attributes: ['nombre'],
                required:false
            },
            ]
        });
    
        return res.status(200).json(pagoDB);
    },

    async hacerPago(req, res){   
        const _id = req.params.id;
        const body = {
           id_estado_pago: 2 
        
        };
        
        try{
            const pagoDB = await Pago.update(body,{
                where: {
                    id_pago: _id
                }
            });
            if(!pagoDB){
               
                return res.status(500).json({mensaje: "Id de pago no encontrado", estado: 0, errores: ""});
            }else{
                return res.status(200).json({mensaje: "Pago actualizado con exito", estado: 1, errores: ""});
            }
        }catch (error){
            return res.status(400).json({
                mensaje: "ocurrio un error",
                error
            })
        }      
    
       
    },

   
}