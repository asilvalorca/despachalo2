import { Perfil } from "../models/index";
import { check, validationResult } from "express-validator";
import { Op } from "sequelize";

module.exports = {
    async crear(req, res){   

        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(422).json({ errores: errors.array() });
        }
        const body = {
            perfil: req.body.perfil,      
        };
       
       
        const buscarPerfil = await Perfil.findAll({
            where: {        
                     perfil: req.body.perfil 
            }
          });
    
          if(buscarPerfil.length > 0){
            return res.status(200).json({mensaje: "El perfil ya existe", estado: 0, errores: ""});
            
          }
        
        try{
            const perfilDB = await Perfil.create(body);
            //res.status(200).json(usuarioDB);
            return res.status(200).json({mensaje: "Perfil guardado con exito", estado: 1, errores: ""});
    
        }catch (error){
            return res.status(500).json({
                mensaje: "ocurrio un error",
                error,
                estado: 0
            })
        }
    },

    async listarPerfiles(req, res){   
        const perfilDB = await Perfil.findAll();        
        return res.status(200).json(perfilDB);
    },

    async listarPerfilesActivos(req, res){   
       
        const perfilDB = await Perfil.findAll({
            where: {        
                    estado: 1 
            }
        });
    
        return res.status(200).json(perfilDB);
    },

    async actualizar(req, res){   
        const _id = req.params.id;
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(422).json({ errores: errors.array() });
        }
        const body = {
           
            perfil: req.body.perfil,
               
        };
          
        const buscarPerfil = await Usuario.findAll({
            where: {
                perfil: req.body.perfil
            }
          });
          if(buscarPerfil.length > 0){
            const id_perfil = buscarPerfil[0].dataValues.id_perfil;
            if(id_perfil != _id){
             res.status(200).json({mensaje: "El perfil ya existe", estado: 0, errores: ""});
             return false;
            }
    
          }
    
        try{
            const perfilDB = await Perfil.update(body,{
                where: {
                    id_perfil: _id
                  }
            });
            if(!perfilDB){
               return res.status(500).json({mensaje: "Id no encontrado", estado: 1, errores: ""});
            }else{
                return  res.status(200).json({mensaje: "Perfil editado con exito", estado: 1, errores: ""});
            }
            
           
        }catch (error){
            return res.status(400).json({
                mensaje: "ocurrio un error",
                error
            })
        }
    },
}