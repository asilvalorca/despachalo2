import { TipoEstado } from "../models/index";
import { check, validationResult } from "express-validator";
import { Op } from "sequelize";

module.exports = {
    async crear(req, res){   

        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(422).json({ errores: errors.array() });
        }
        const body = {
            tipoestado: req.body.tipoestado,      
        };
       
       
        const buscarTipoEstado = await TipoEstado.findAll({
            where: {        
                     tipoestado: req.body.tipoestado 
            }
          });
    
          if(buscarTipoEstado.length > 0){
            return res.status(200).json({mensaje: "El tipoestado ya existe", estado: 0, errores: ""});
          
          }
        
        try{
            const tipoestadoDB = await TipoEstado.create(body);
            //res.status(200).json(usuarioDB);
            res.status(200).json({mensaje: "TipoEstado guardado con exito", estado: 1, errores: ""});
    
        }catch (error){
            return res.status(500).json({
                mensaje: "ocurrio un error",
                error,
                estado: 0
            })
        }
    },

    async listarTipoEstado(req, res){   
        const tipoestadoDB = await TipoEstado.findAll();
        return res.status(200).json(tipoestadoDB);
    },

    async listarTipoEstadoActivos(req, res){   
        const tipoestadoDB = await TipoEstado.findAll({
            where: {        
                     estado: 1 
            }
          });
        return res.status(200).json(tipoestadoDB);
    },

    async actualizar(req, res){   
        const _id = req.params.id;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(422).json({ errores: errors.array() });
    }
    const body = {
       
        tipoestado: req.body.tipoestado,
           
    };
      
    const buscarTipoEstado = await TipoEstado.findAll({
        where: {
            tipoestado: req.body.tipoestado
        }
      });
      if(buscarTipoEstado.length > 0){
        const id_tipo_estado = buscarTipoEstado[0].dataValues.id_tipoestado;
        if(id_tipo_estado != _id){
         res.status(200).json({mensaje: "El tipoestado ya existe", estado: 0, errores: ""});
         return false;
        }

      }

    try{
        const tipoestadoDB = await TipoEstado.update(body,{
            where: {
                id_tipo_estado: _id
              }
        });
        if(!tipoestadoDB){
            return res.status(500).json({mensaje: "Id no encontrado", estado: 1, errores: ""});
        }else{
            return res.status(200).json({mensaje: "TipoEstado editado con exito", estado: 1, errores: ""});
        }
        
       
    }catch (error){
        return res.status(400).json({
            mensaje: "ocurrio un error",
            error
        })
    }
    },
}