import { Comuna } from "../models/index";
import { check, validationResult } from "express-validator";
import { Op } from "sequelize";

module.exports = {
    async crear(req, res){   

        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(422).json({ errores: errors.array() });
        }
        const body = {
                comuna: req.body.comuna,      
        };
           
           
        const buscarComuna = await Comuna.findAll({
            where: {        
                comuna: req.body.comuna 
            }
        });
        
        if(buscarComuna.length > 0){
            return  res.status(200).json({mensaje: "El comuna ya existe", estado: 0, errores: ""});
               
        }
        body.precio = req.body.precio!=''?req.body.precio:0;
        try{
            const comunaDB = await Comuna.create(body);
                //res.status(200).json(usuarioDB);
            return res.status(200).json({mensaje: "Comuna guardada con exito", estado: 1, errores: ""});
        
        }catch (error){
            return res.status(500).json({
                mensaje: "ocurrio un error",
                 error,
                estado: 0
            })
            
        }
    },

    async listarComunas(req, res){   
        const comunaDB = await Comuna.findAll();
        return res.status(200).json(comunaDB);
    },

    async listarComunasActivas(req, res){   
        const comunaDB = await Comuna.findAll({
            where: {        
                     estado: 1 
            }
          });
      
        return  res.status(200).json(comunaDB);
    },

    async actualizar(req, res){   
        const _id = req.params.id;
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(422).json({ errores: errors.array() });
        }
        const body = {
           
            comuna: req.body.comuna,
            estado: req.body.estado,
               
        };
          
        const buscarComuna = await Comuna.findAll({
            where: {
                comuna: req.body.comuna
            }
          });
          if(buscarComuna.length > 0){
            const id_comuna = buscarComuna[0].dataValues.id_comuna;
            if(id_comuna != _id){
                return res.status(200).json({mensaje: "El comuna ya existe", estado: 0, errores: ""});
             
            }
    
          }
          body.precio = req.body.precio!=''?req.body.precio:0;
        try{
            const comunaDB = await Comuna.update(body,{
                where: {
                    id_comuna: _id
                  }
            });
            if(!comunaDB){
                return res.status(500).json({mensaje: "Id no encontrado", estado: 1, errores: ""});
            }else{
                return res.status(200).json({mensaje: "Comuna editada con exito", estado: 1, errores: ""});
            }
            
           
        }catch (error){
            return res.status(400).json({
                mensaje: "ocurrio un error",
                error
            })
        }
    },
}