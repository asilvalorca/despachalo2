import { Envio, Usuario, Comuna, EnvioDetalle, TipoEstado, Empresa, Pago } from "../models/index";
import { check, validationResult } from "express-validator";
import { Op } from "sequelize";
import xlsx from "xlsx"; 
import nodemailer from "nodemailer";

 const EnvioCorreo = async(correo) =>{
     console.log(correo);
     console.log("entra al correo");
    const htm = 
    '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'+
    '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" style="width:100%;font-family:roboto, helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">'+
     '<head> '+
      '<meta charset="UTF-8"> '+
      '<meta content="width=device-width, initial-scale=1" name="viewport"> '+
      '<meta name="x-apple-disable-message-reformatting"> '+
      '<meta http-equiv="X-UA-Compatible" content="IE=edge"> '+
      '<meta content="telephone=no" name="format-detection"> '+
      '<title>Nueva plantilla de correo electrónico 2021-01-13</title> '+
      '<!--[if (mso 16)]>'+
        '<style type="text/css">'+
        'a {text-decoration: none;}'+
        '</style>'+
        '<![endif]--> '+
      '<!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> '+
      '<!--[if gte mso 9]>'+
    '<xml>'+
        '<o:OfficeDocumentSettings>'+
        '<o:AllowPNG></o:AllowPNG>'+
        '<o:PixelsPerInch>96</o:PixelsPerInch>'+
        '</o:OfficeDocumentSettings>'+
    '</xml>'+
    '<![endif]--> '+
      '<!--[if !mso]><!-- --> '+
      '<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet">'+ 
      '<!--<![endif]--> '+
      '<style type="text/css">'+
    '.rollover div {'+
        'font-size:0;'+
    '}'+
    '#outlook a {'+
        'padding:0;'+
    '}'+
    '.ExternalClass {'+
        'width:100%;'+
    '}'+
    '.ExternalClass,'+
    '.ExternalClass p,'+
    '.ExternalClass span,'+
    '.ExternalClass font,'+
    '.ExternalClass td,'+
    '.ExternalClass div {'+
        'line-height:100%;'+
    '}'+
    '.es-button {'+
        'mso-style-priority:100!important;'+
        'text-decoration:none!important;'+
    '}'+
    'a[x-apple-data-detectors] {'+
        'color:inherit!important;'+
        'text-decoration:none!important;'+
        'font-size:inherit!important;'+
        'font-family:inherit!important;'+
        'font-weight:inherit!important;'+
        'line-height:inherit!important;'+
    '}'+
    '.es-desk-hidden {'+
        'display:none;'+
        'float:left;'+
        'overflow:hidden;'+
        'width:0;'+
        'max-height:0;'+
        'line-height:0;'+
        'mso-hide:all;'+
    '}'+
    '@media only screen and (max-width:600px) {p, ul li, ol li, a { font-size:14px!important; line-height:150%!important } h1 { font-size:25px!important; text-align:center; line-height:120%!important } h2 { font-size:20px!important; text-align:center; line-height:120%!important } h3 { font-size:15px!important; text-align:center; line-height:120%!important } h1 a { font-size:25px!important } h2 a { font-size:20px!important } h3 a { font-size:15px!important } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:13px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:12px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } a.es-button, button.es-button { font-size:14px!important; display:block!important; border-left-width:0px!important; border-right-width:0px!important } }'+
    '</style>'+ 
     '</head> '+
     '<body style="width:100%;font-family:roboto,  helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0"> '+
      '<div class="es-wrapper-color" style="background-color:#FFFFFF"> '+
       '<!--[if gte mso 9]>'+
                '<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">'+
                    '<v:fill type="tile" color="#ffffff"></v:fill>'+
                '</v:background>'+
            '<![endif]--> '+
       '<table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:no-repeat;background-position:left top"> '+ 
         '<tr style="border-collapse:collapse"> '+
          '<td valign="top" style="padding:0;Margin:0"> '+         
           '<table class="es-header" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"> '+ 
             '<tr style="border-collapse:collapse"> '+ 
              '<td align="center" style="padding:0;Margin:0;background-color:transparent" bgcolor="transparent"> '+ 
               '<table class="es-header-body" cellspacing="0" cellpadding="0" bgcolor="#524638" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#524638;background-repeat:no-repeat;width:600px;background-image:url(https://otqwug.stripocdn.email/content/guids/76c7f620-fcdd-469a-995e-f695f16cbcaa/images/92231610565022680.jpg);background-position:left top" background="https://otqwug.stripocdn.email/content/guids/76c7f620-fcdd-469a-995e-f695f16cbcaa/images/92231610565022680.jpg"> '+ 
                 '<tr style="border-collapse:collapse"> '+ 
                  '<td align="left" bgcolor="#0b5394" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;background-color:#0B5394"> '+ 
                   '<table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '+ 
                     '<tr style="border-collapse:collapse"> '+ 
                      '<td valign="top" align="center" style="padding:0;Margin:0;width:560px"> '+ 
                       '<table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '+ 
                         '<tr style="border-collapse:collapse"> '+ 
                          '<td align="center" style="padding:0;Margin:0;padding-top:10px;font-size:0px"><a target="_blank" href="https://despachalochile.cl/" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto,  helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:#1376C8"><img src="https://otqwug.stripocdn.email/content/guids/76c7f620-fcdd-469a-995e-f695f16cbcaa/images/56211610565042042.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="100"></a></td> '+ 
                         '</tr> '+ 
                       '</table></td> '+ 
                     '</tr> '+ 
                   '</table></td> '+ 
                 '</tr> '+ 
                 '<tr style="border-collapse:collapse"> '+ 
                  '<td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;background-color:transparent" bgcolor="transparent"> '+ 
                   '<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '+ 
                     '<tr style="border-collapse:collapse"> '+ 
                      '<td align="center" valign="top" style="padding:0;Margin:0;width:560px"> '+ 
                       '<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-position:center top" role="presentation"> '+ 
                         '<tr style="border-collapse:collapse"> '+ 
                          '<td align="center" height="130" style="padding:0;Margin:0"></td> '+ 
                         '</tr> '+ 
                       '</table></td> '+ 
                     '</tr> '+ 
                   '</table></td> '+ 
                 '</tr> '+ 
               '</table></td> '+ 
             '</tr> '+ 
           '</table> '+ 
           '<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"> '+ 
             '<tr style="border-collapse:collapse"> '+ 
              '<td align="center" style="padding:0;Margin:0"> '+ 
               '<table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"> '+ 
                 '<tr style="border-collapse:collapse"> '+ 
                  '<td align="left" style="padding:0;Margin:0;padding-bottom:10px;padding-left:20px;padding-right:20px"> '+ 
                   '<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '+ 
                     '<tr style="border-collapse:collapse"> '+ 
                      '<td align="left" style="padding:0;Margin:0;width:560px"> '+ 
                       '<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;border-spacing:0px;border-radius:5px" role="presentation"> '+ 
                         '<tr style="border-collapse:collapse"> '+ 
                          '<td align="center" height="44" style="padding:0;Margin:0"></td> '+ 
                         '</tr> '+ 
                         '<tr style="border-collapse:collapse"> '+ 
                          '<td align="left" style="padding:0;Margin:0"><h2 style="Margin:0;line-height:120%;mso-line-height-rule:exactly;font-family:roboto,  helvetica, arial, sans-serif">Tu envío&nbsp;<strong>'+correo.estado+'</strong></h2></td> '+ 
                         '</tr> '+ 
                         '<tr style="border-collapse:collapse"> '+ 
                          '<td align="left" style="padding:0;Margin:0;padding-top:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, helvetica neue, helvetica, arial, sans-serif;line-height:21px;color:#333333"><strong>'+correo.nombre.toUpperCase()+'</strong>, gracias por recibirnos.<br><br>Tu envío número&nbsp;<strong>'+correo.id_envio+'</strong>&nbsp;fue entregado exitosamente a andres silva en&nbsp;<strong>El Azafran 16401, San Bernardo</strong>&nbsp;a las 12:58.</p></td> '+ 
                         '</tr> '+ 
                       '</table></td> '+ 
                     '</tr> '+ 
                   '</table></td> '+ 
                 '</tr> '+ 
                 '<tr style="border-collapse:collapse">'+  
                  '<td align="left" style="padding:0;Margin:0;background-position:center top;background-color:#EEF4F4" bgcolor="#eef4f4"> '+ 
                   '<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '+ 
                     '<tr style="border-collapse:collapse"> '+ 
                      '<td align="center" valign="top" style="padding:0;Margin:0;width:600px"> '+ 
                       '<table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '+ 
                         '<tr style="border-collapse:collapse"> '+ 
                          '<td align="center" style="padding:0;Margin:0;font-size:0"><img class="adapt-img" src="https://otqwug.stripocdn.email/content/guids/CABINET_16d686acb70b9779d929f9ccfe93f5e6/images/74001567514648241.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="600"></td> '+ 
                         '</tr> '+ 
                       '</table></td> '+ 
                     '</tr> '+ 
                   '</table></td> '+ 
                 '</tr> '+ 
               '</table></td> '+ 
             '</tr> '+ 
           '</table> '+ 
           '<table cellpadding="0" cellspacing="0" class="es-footer" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"> '+
             '<tr style="border-collapse:collapse"> '+ 
              '<td align="center" style="padding:0;Margin:0;background-color:transparent" bgcolor="transparent"> '+
               '<table bgcolor="#FFFFFF" class="es-footer-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;width:600px"> '+
                 '<tr style="border-collapse:collapse"> '+
                  '<td align="left" bgcolor="#0b5394" style="Margin:0;padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px;background-color:#0B5394"> '+
                   '<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '+
                     '<tr style="border-collapse:collapse"> '+
                      '<td align="center" valign="top" style="padding:0;Margin:0;width:560px"> '+
                       '<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-position:left top" role="presentation"> '+
                         '<tr style="border-collapse:collapse"> '+
                          '<td align="center" class="es-m-txt-c" style="padding:0;Margin:0;padding-top:15px;padding-bottom:15px;font-size:0"> '+
                           '<table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> '+
                             '<tr style="border-collapse:collapse"> '+
                              '<td align="center" valign="top" style="padding:0;Margin:0;padding-right:15px"><a href="www.instagram.com/despachalochile"><img src="https://otqwug.stripocdn.email/content/assets/img/social-icons/circle-black/instagram-circle-black.png" alt="Ig" title="Instagram" width="24" height="24" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a></td> '+
                               '</tr> '+
                           '</table></td> '+
                         '</tr> '+
                         '<tr style="border-collapse:collapse"> '+
                          '<td align="center" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:roboto, helvetica, arial, sans-serif;line-height:20px;color:#FFFFFF">When building this template, we took all content from Etsy.com to adjust product cards with smart-elements to Etsy.</p></td> '+
                         '</tr> '+
                      '</table></td> '+
                     '</tr> '+
                   '</table></td> '+
                 '</tr> '+
               '</table></td> '+
            '</tr> '+
           '</table> '+
           '</td> '+
         '</tr> '+
       '</table> '+
      '</div>  '+
     '</body>'+
    '</html>';
    
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: "mail.uman.cl",
            port: 25,
            secure: false, // true for 465, false for other ports
            auth: {
              user: 'veladero@uman.cl', // generated ethereal user
              pass: 'veladero2018', // generated ethereal password
            },
            tls:{rejectUnauthorized: false}
          });
        
          // send mail with defined transport object
          let info = await transporter.sendMail({
            from: '"Despachalo "sistema.compras@sistemas.bailac.cl', // sender address
            to: "a.silvalorca@gmail.com", // list of receivers
            subject: "Información de despacho", // Subject line
            text: "Hello world?", // plain text body
            html: htm, // html body
          });
        
          console.log("Message sent: %s", info.messageId);
          // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
        
          // Preview only available when sending through an Ethereal account
          console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
          // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
          /*
    async function main() {
        
      
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
          host: "mail.uman.cl",
          port: 25,
          secure: false, // true for 465, false for other ports
          auth: {
            user: 'veladero@uman.cl', // generated ethereal user
            pass: 'veladero2018', // generated ethereal password
          },
          tls:{rejectUnauthorized: false}
        });
      
        // send mail with defined transport object
        let info = await transporter.sendMail({
          from: '"Despachalo "sistema.compras@sistemas.bailac.cl', // sender address
          to: "a.silvalorca@gmail.com", // list of receivers
          subject: "Información de despacho", // Subject line
          text: "Hello world?", // plain text body
          html: htm, // html body
        });
      
        console.log("Message sent: %s", info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
      
        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
      }
      */
     // main().catch(console.error);
     
}
module.exports = {
    async correo(req, res){
        await EnvioCorreo();
        return res.status(200).json({mensaje: "Envio guardado con exito", estado: 1, errores: ""});
    },
    async crear(req, res){

        console.log("nuevo envio");

        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(422).json({ errores: errors.array() });
        }   

        const buscarComuna = await Comuna.findOne({
            where: {        
                id_comuna: req.body.id_comuna, 
            }
        });

        const body = {
           

            id_empresa: req.body.id_empresa,
            id_comuna: req.body.id_comuna,
            numero: 0,
            correo: req.body.correo,
            //rut: req.body.rut,    
            monto: buscarComuna.precio, 
            comentario: req.body.comentario,
            telefono: req.body.telefono,  
            id_conductor: null,  
            direccion: req.body.direccion,
            id_usuario: null,
            nombre_destinatario:  req.body.nombre_destinatario,
            id_tipo_estado: 1,
            //fecha_compra: req.body.fecha_compra,  
            //fecha_entrega: req.body.fecha_entrega,  
          
        };
       
        
        /*
        const buscarEnvio = await Envio.findAll({
            where: {
                [Op.and]: [
                    { numero: req.body.numero },
                    { id_empresa: req.body.id_empresa }
                  ]
            }
          });
          if(buscarEnvio.length > 0){
            return res.status(403).json({mensaje: "El envio ya existe", estado: 0, errores: ""});
            next();
          }*/
        
        try{
            const envioDB = await Envio.create(body);
            envioDB.dataValues.comuna = buscarComuna.comuna;
            const envioDetalleDB = await EnvioDetalle.create({
                id_envio: envioDB.id_envio,
                id_usuario: null,
                id_tipo_estado: 1,

            });

            const pagoDB = await Pago.create({
                monto: buscarComuna.precio, 
                id_envio : envioDB.id_envio, 
                id_empresa : req.body.id_empresa, 
                id_estado_pago: 1
            });

            console.log(envioDB);
            return res.status(200).json({mensaje: "Envio guardado con exito", estado: 1, errores: "", data: envioDB});
        }catch (error){
            return res.status(500).json({
                mensaje: "ocurrio un error",
                error,
                estado: 0
            })
        }
    },
    
    async cargaConductor(req, res){   
        
        const _id = req.params.id;
        if(!_id ||  _id==""){
            
        }
        const envioDB = await Envio.findAll({ 
            where: {        
                id_conductor: _id, 
                id_tipo_estado : [3,4,5,6,7]
            }, 
            include:[
                {
                    model: Empresa,
                    as: "Empresa",
                    attributes: ['nombre'],
                    required:false
                },{
                    model: Comuna,
                    as: "Comuna",
                    attributes: ['comuna'],
                    required:false
                },{
                    model: Usuario,
                    as: "Usuario",
                    attributes: ['nombre'],
                    required:false
                },
                {
                    model: Usuario,
                    as: "UsuarioConductor",
                    attributes: ['nombre'],
                    required:false
                },{
                    model: TipoEstado,
                    as: "TipoEstado",
                    attributes: ['tipoestado'],
                    required:false
                }
            ]
        });
        return res.status(200).json(envioDB);
    },
    async listarEnvios(req, res){   
        
        const envioDB = await Envio.findAll({ include:[
        {
            model: Empresa,
            as: "Empresa",
            attributes: ['nombre'],
            required:false
        },{
            model: Comuna,
            as: "Comuna",
            attributes: ['comuna'],
            required:false
        },{
            model: Usuario,
            as: "Usuario",
            attributes: ['nombre'],
            required:false
        },
        {
            model: Usuario,
            as: "UsuarioConductor",
            attributes: ['nombre'],
            required:false
        },{
            model: TipoEstado,
            as: "TipoEstado",
            attributes: ['tipoestado'],
            required:false
        }]
        });
    
        return res.status(200).json(envioDB);
    },

    async listarEnviosUsuario(req, res){   
        const _id = req.params.id;

        const buscarUsuario = await Usuario.findOne({
            where: {
                id_usuario : _id
            }
        });
        console.log(buscarUsuario);
       
        if(buscarUsuario.id_perfil==1){
            const envioDB = await Envio.findAll({ 
            include:[
                {
                    model: Empresa,
                    as: "Empresa",
                    attributes: ['nombre'],
                    required:false
                },{
                    model: Comuna,
                    as: "Comuna",
                    attributes: ['comuna'],
                    required:false
                },{
                    model: Usuario,
                    as: "Usuario",
                    attributes: ['nombre'],
                    required:false
                },
                {
                    model: Usuario,
                    as: "UsuarioConductor",
                    attributes: ['nombre'],
                    required:false
                },{
                    model: TipoEstado,
                    as: "TipoEstado",
                    attributes: ['tipoestado'],
                    required:false
                }]
                });
                return res.status(200).json(envioDB);
            
        }else if(buscarUsuario.id_perfil==3){
            const envioDB = await Envio.findAll({ where: {        
                id_empresa: buscarUsuario.id_empresa, 
            },
            include:[
                {
                    model: Empresa,
                    as: "Empresa",
                    attributes: ['nombre'],
                    required:false
                },{
                    model: Comuna,
                    as: "Comuna",
                    attributes: ['comuna'],
                    required:false
                },{
                    model: Usuario,
                    as: "Usuario",
                    attributes: ['nombre'],
                    required:false
                },
                {
                    model: Usuario,
                    as: "UsuarioConductor",
                    attributes: ['nombre'],
                    required:false
                },{
                    model: TipoEstado,
                    as: "TipoEstado",
                    attributes: ['tipoestado'],
                    required:false
                }]
                });
                return res.status(200).json(envioDB);
        }else{
            return res.status(500).json({mensaje: "Perfil de usuario no puede ver esto", estado:0});
        }
        
           
    },
    async buscarEnvio(req, res){   
        const _id = req.params.id;
       const envioDB = await Envio.findAll({where: {
            id_envio: _id
        },
         include:[
        {
            model: Empresa,
            as: "Empresa",
            attributes: ['nombre'],
            required:false
        },{
            model: Comuna,
            as: "Comuna",
            attributes: ['comuna'],
            required:false
        },{
            model: Usuario,
            as: "Usuario",
            attributes: ['nombre'],
            required:false
        },
        {
            model: Usuario,
            as: "UsuarioConductor",
            attributes: ['nombre'],
            required:false
        },{
            model: TipoEstado,
            as: "TipoEstado",
            attributes: ['tipoestado'],
            required:false
        }]
        });
    
       const envioDetalleDB = await EnvioDetalle.findAll({where: {
        id_envio: _id
        },
        include:[
        {
            model: TipoEstado,
            as: "TipoEstado",
            attributes: ['tipoestado'],
            required:false
        
        }]
        });
        return res.status(200).json({envio:envioDB, detalle: envioDetalleDB});
    },

    async cambiarEstado(req, res){   
        const _id = req.params.id;     
        console.log(_id);
        console.log(req.body.estado);
        const body = {         
            id_tipo_estado: req.body.estado,         
        };              
    
      
        try{
           
            const envioDB = await Envio.update(body,{
                where: {
                    id_envio: _id
                  }
            });
            const envioDetalleDB = await EnvioDetalle.create({
                id_envio: _id,
                id_usuario: null,
                id_tipo_estado: req.body.estado,
                comentario: req.body.comentario!=""?req.body.comentario : null,
            });

            const ConsultaEnvio = await Envio.findOne({
                where: {
                    id_envio: _id
                  },                  
                include:[{
                    model: TipoEstado,
                    as: "TipoEstado",
                    attributes: ['tipoestado'],
                    required:false
                }]
            });
            console.log(ConsultaEnvio);
            const email = {
                id_envio : _id,
                id_estado : ConsultaEnvio.id_tipo_estado,
                estado : ConsultaEnvio.TipoEstado.tipoestado,
                correo : ConsultaEnvio.correo,
                   nombre : ConsultaEnvio.nombre_destinatario,

            }
            if(!envioDB){
               
                return res.status(500).json({mensaje: "Id no encontrado", estado: 0, errores: ""});
            }else{
                EnvioCorreo(email);
                return res.status(200).json({mensaje: "Cambio de estado actualizado con exito", estado: 1, errores: "", data: envioDB});
            }
        }catch (error){
            return res.status(500).json({
                mensaje: "ocurrio un error",
                error,
                estado: 0
            })
        }
    },
    async cambiarConductor(req, res){   
        const _id = req.params.id;
        const body = {         
            id_conductor: req.body.id_conductor,     
            id_tipo_estado: 3   
        };           
    
        
        try{
           
            const envioDB = await Envio.update(body,{
                where: {
                    id_envio: _id
                  }
            });
            const envioDetalleDB = await EnvioDetalle.create({
                id_envio: _id,
                id_usuario: null,
                id_tipo_estado: 3,

            });

            if(!envioDB){
                return res.status(500).json({mensaje: "Id no encontrado", estado: 0, errores: ""});
            }else{
                return res.status(200).json({mensaje: "Cambio de estado actualizado con exito", estado: 1, errores: "", data: envioDB});
            }
        }catch (error){
            return res.status(500).json({
                mensaje: "ocurrio un error",
                error,
                estado: 0
            })
        }
    },
    async actualizar(req, res){   
        const _id = req.params.id;
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(422).json({ errores: errors.array() });
        }

        
        const buscarComuna = await Comuna.findOne({
            where: {        
                id_comuna: req.body.id_comuna, 
            }
        });

        const body = { 

           
            id_comuna: req.body.id_comuna,            
            correo: req.body.correo,
            //rut: req.body.rut,    
            monto: buscarComuna.precio, 
            comentario: req.body.comentario,
            telefono: req.body.telefono,          
            direccion: req.body.direccion,
            id_usuario: null,
            nombre_destinatario:  req.body.nombre_destinatario,            
            //fecha_compra: req.body.fecha_compra,  
            //fecha_entrega: req.body.fecha_entrega,           
        };       
        
    
        
        try{
           
            const envioDB = await Envio.update(body,{
                where: {
                    id_envio: _id
                  }
            });

            if(!envioDB){
                return res.status(500).json({mensaje: "Id no encontrado", estado: 0, errores: ""});
            }else{
                return res.status(200).json({mensaje: "Envio editada con exito", estado: 1, errores: "", data: envioDB});
            }
        }catch (error){
            return res.status(500).json({
                mensaje: "ocurrio un error",
                error,
                estado: 0
            })
        }
    },
   
    async cargarEnvio(req, res){  
        
        console.log("request");
        console.log(req.body);
        const empresa = req.body.empresa;
        if(!empresa){
         
            return res.status(500).json({mensaje: "Debe enviar el id de la empresa", estado: 0, errores: ""});
        }
        if(!req.file){
         
            return res.status(500).json({mensaje: "Problema con el formato del archivo", estado: 0, errores: ""});
        }

        const nombreArchivo = req.file.filename;
        const workbook = xlsx.readFile("archivos/"+nombreArchivo);
        const workbookSheets = workbook.SheetNames;
        const sheet = workbookSheets[0];
        const dataExcel = await xlsx.utils.sheet_to_json(workbook.Sheets[sheet]);
        console.log(dataExcel);
        const incorrectas = [];
        const correctas = [];


       // const procesando = await procesarEnvio(dataExcel);
       // console.log("Proceso", procesando);

        //const result = await dataExcel.forEach(async (element) => {
            console.log("antes");
            for (let element of dataExcel) {  
            const buscarComuna = await Comuna.findOne({
                where: {        
                    comuna: element.comuna 
                }
            });

            if(buscarComuna){
                const body = {
                    id_empresa: empresa,
                    id_comuna: buscarComuna.id_comuna,
                    numero: 0,
                    correo: element.correo,
                    rut: 0,    
                    monto: buscarComuna.precio,  
                    telefono:  element.telefono, 
                    id_conductor: null,  
                    direccion: element.direccion,
                    id_usuario: 1,
                    nombre_destinatario:  element.nombre,
                    id_tipo_estado: 1,
                   
                }
                //console.log(body);
                const envioDB = await Envio.create(body);
                const envioDetalleDB = await EnvioDetalle.create({
                    id_envio: envioDB.id_envio,
                    id_usuario: null,
                    id_tipo_estado: 1,
    
                });
                envioDB.dataValues.comuna = buscarComuna.comuna;
                console.log(envioDB.dataValues);
                correctas.push(envioDB.dataValues);
                console.log(correctas);

                const pagoDB = await Pago.create({
                    monto: buscarComuna.precio, 
                    id_envio : envioDB.id_envio, 
                    id_empresa : empresa, 
                    id_estado_pago: 1
                });
               
            }else{
                incorrectas.push(element);
            }       
            
        }
        console.log("correctas");
    
        return res.status(200).json({buenas: correctas, malas: incorrectas});
        
    },
}