import { Zona, Comuna } from "../models/index";
import { check, validationResult } from "express-validator";
import { Op } from "sequelize";

module.exports = {
    async crear(req, res){   

        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(422).json({ errores: errors.array() });
        }
        const body = {
            zona: req.body.zona,      
        };
       
       
        const buscarZona = await Zona.findAll({
            where: {        
                     zona: req.body.zona 
            }
          });
    
          if(buscarZona.length > 0){
            return res.status(200).json({mensaje: "El zona ya existe", estado: 0, errores: ""});
             ;
          }
        
        try{
            const zonaDB = await Zona.create(body);
            //res.status(200).json(usuarioDB);
            return res.status(200).json({mensaje: "Zona guardado con exito", estado: 1, errores: ""});
    
        }catch (error){
            return res.status(500).json({
                mensaje: "ocurrio un error",
                error,
                estado: 0
            })
        }
    },

    async listarZonas(req, res){   
       
        const zonaDB = await Zona.findAll({
            include:[{
                model: Comuna,
                as: "Comunas",
                attributes: ['comuna'],
                through: {
                    attributes: ['zona_id_zona', 'comuna_id_comuna'],
                  }
                }]
        });
        return res.status(200).json(zonaDB);
    },

    async listarZonasActivas(req, res){   
        const zonaDB = await Zona.findAll({
            where: {        
                     estado: 1 
            },include:[{
                model: Comuna,
                as: "Comunas",
                attributes: ['comuna'],
                through: {
                    attributes: ['zona_id_zona', 'comuna_id_comuna'],
                  }
                }]
          });
        
        return res.status(200).json(zonaDB);
    },

    async actualizar(req, res){   
        const _id = req.params.id;
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(422).json({ errores: errors.array() });
        }
        const body = {
           
            zona: req.body.zona,
            estado: req.body.estado,
        };
          
        const buscarZona = await Zona.findAll({
            where: {
                zona: req.body.zona
            }
          });
          if(buscarZona.length > 0){
            const id_zona = buscarZona[0].dataValues.id_zona;
            if(id_zona != _id){
                return res.status(200).json({mensaje: "El zona ya existe", estado: 0, errores: ""});
              
            }
    
          }
    
        try{
            const zonaDB = await Zona.update(body,{
                where: {
                    id_zona: _id
                  }
            });
            if(!zonaDB){
                return res.status(500).json({mensaje: "Id no encontrado", estado: 1, errores: ""});
            }else{
                return res.status(200).json({mensaje: "Zona editada con exito", estado: 1, errores: ""});
            }
            
           
        }catch (error){
            return res.status(400).json({
                mensaje: "ocurrio un error",
                error
            })
        }
    },
}