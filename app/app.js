import express from "express";
import morgan from "morgan";
import cors from "cors";
import path from "path";
//import {connection}  from "../database/db";
import {sequelize}  from "./models/index";

const app = express();

app.use(cors());
app.use(morgan("tiny"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/api', require("../routes/index"));



app.set('port',process.env.PORT || 5000 );
app.listen(app.get("port"),()=>{

    console.log("Server en el puerto",app.get("port"));
    sequelize.sync({alter: true}).then(()=>{
          console.log("Conectado a la base de datos");

          
     }).catch(error =>{
          console.log("se ha producido un error", error);
     })


});