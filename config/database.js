require('dotenv').config();

module.exports ={
  username: process.env.DV_USERNAME || "admin_despachalo",
  password:  process.env.DB_PASSWORD || "3ibvMysMq4eJ7b76",
  database:  process.env.DB_DATABASE || "admin_despachalo",
  host:  process.env.DB_HOST ||  "localhost",
  dialect:  process.env.DB_DIALECT ||  "mysql",
  define:{
    underscored: true
  }
}