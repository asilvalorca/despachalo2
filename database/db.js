import { Sequelize, DataTypes }  from 'sequelize';
import config  from "../config/database";
const db = {};
db.connection = new Sequelize(
    config.database,
    config.username,
    config.password,
    {
        host: config.host,
        dialect: config.dialect,
    }
);
db.Comuna = require("../app/models/Comuna")(db.connection, DataTypes);
db.Empresa = require("../app/models/Empresa")(db.connection, DataTypes);
db.Envio = require("../app/models/Envio")(db.connection, DataTypes);
db.Perfil = require("../app/models/Perfil")(db.connection, DataTypes);
db.TipoEstado = require("../app/models/TipoEstado")(db.connection, DataTypes);
db.Usuario = require("../app/models/Usuario")(db.connection, DataTypes);
db.Zona = require("../app/models/Zona")(db.connection, DataTypes);
db.Empresa.associate(db);
db.Usuario.associate(db);
db.Zona.associate(db);
module.exports = db;