'use strict';
const faker = require("faker");

module.exports = {
  up: async (queryInterface, Sequelize) => {

    let enviodetalle =[];
    const hoy = new Date();
    const fecha = hoy.getFullYear() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getDate();
    const hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
    const fechaHora = fecha+" "+hora;

    enviodetalle.push({producto : faker.random.word(), codigo_producto: faker.random.alphaNumeric(), cantidad: faker.random.number(), monto: faker.random.number(), id_envio: 1, created_at: fechaHora, updated_at: fechaHora})
    enviodetalle.push({producto : faker.random.word(), codigo_producto: faker.random.alphaNumeric(), cantidad: faker.random.number(), monto: faker.random.number(), id_envio: 1, created_at: fechaHora, updated_at: fechaHora})
    enviodetalle.push({producto : faker.random.word(), codigo_producto: faker.random.alphaNumeric(), cantidad: faker.random.number(), monto: faker.random.number(), id_envio: 1, created_at: fechaHora, updated_at: fechaHora})
    
    enviodetalle.push({producto : faker.random.word(), codigo_producto: faker.random.alphaNumeric(), cantidad: faker.random.number(), monto: faker.random.number(), id_envio: 2, created_at: fechaHora, updated_at: fechaHora})
    enviodetalle.push({producto : faker.random.word(), codigo_producto: faker.random.alphaNumeric(), cantidad: faker.random.number(), monto: faker.random.number(), id_envio: 2, created_at: fechaHora, updated_at: fechaHora})
    enviodetalle.push({producto : faker.random.word(), codigo_producto: faker.random.alphaNumeric(), cantidad: faker.random.number(), monto: faker.random.number(), id_envio: 2, created_at: fechaHora, updated_at: fechaHora})
    
    enviodetalle.push({producto : faker.random.word(), codigo_producto: faker.random.alphaNumeric(), cantidad: faker.random.number(), monto: faker.random.number(), id_envio: 3, created_at: fechaHora, updated_at: fechaHora})
    enviodetalle.push({producto : faker.random.word(), codigo_producto: faker.random.alphaNumeric(), cantidad: faker.random.number(), monto: faker.random.number(), id_envio: 3, created_at: fechaHora, updated_at: fechaHora})
    enviodetalle.push({producto : faker.random.word(), codigo_producto: faker.random.alphaNumeric(), cantidad: faker.random.number(), monto: faker.random.number(), id_envio: 3, created_at: fechaHora, updated_at: fechaHora})
     
   // await queryInterface.bulkInsert('envio_detalles', enviodetalle, {}); 
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
