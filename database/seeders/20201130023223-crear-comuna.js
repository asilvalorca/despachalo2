'use strict';
const faker = require("faker");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
   let comunas =[];
   const hoy = new Date();
   const fecha = hoy.getFullYear() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getDate();
   const hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
   const fechaHora = fecha+" "+hora;
 
    comunas.push({comuna : 'CERRILLOS', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'RENCA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'MAIPU', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'CERRO NAVIA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'ESTACION CENTRAL', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'QUINTA NORMAL', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'LO PRADO', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'PUDAHUEL', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})

    comunas.push({comuna : 'SANTIAGO', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'QUILICURA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'RECOLETA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'HUECHURABA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'INDEPENDENCIA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'CONCHALI', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})


    comunas.push({comuna : 'VITACURA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'LO BARNECHEA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'ÑUÑOA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'LAS CONDES', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'PROVIDENCIA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'LA REINA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'PEÑALOLEN', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})

    comunas.push({comuna : 'LA PINTANA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'LA GRANJA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'SAN MIGUEL', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'SAN RAMON', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'PEDRO AGUIRRES CERDA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'LO ESPEJO', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'LA FLORIDA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'SAN JOAQUIN', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'MACUL', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'PUENTE ALTO', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'EL BOSQUE', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'LA CISTERNA', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})
    comunas.push({comuna : 'SAN BERNARDO', precio : 3570, 	created_at: fechaHora, updated_at: fechaHora})


   
   await queryInterface.bulkInsert('comunas', comunas, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
