'use strict';
const faker = require("faker");

module.exports = {
  up: async (queryInterface, Sequelize) => {

    let envios = [];
    const hoy = new Date();
    const fecha = hoy.getFullYear() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getDate();
    const hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
    const fechaHora = fecha+" "+hora;

    envios.push({monto : faker.random.number(), nombre_destinatario: faker.name.firstName(), rut: faker.random.number(), telefono: 4542112, direccion: faker.address.streetAddress(), numero: faker.random.number(), correo: faker.internet.email(), comentario: faker.lorem.words(), fecha_compra: fecha, fecha_entrega: fecha, id_empresa: 1, id_usuario: 1, id_conductor: 2, id_comuna: 1, id_tipo_estado: 1, created_at: fechaHora, updated_at: fechaHora});
    envios.push({monto : faker.random.number(), nombre_destinatario: faker.name.firstName(), rut: faker.random.number(), telefono: 4542112, direccion: faker.address.streetAddress(), numero: faker.random.number(), correo: faker.internet.email(), comentario: faker.lorem.words(), fecha_compra: fecha, fecha_entrega: fecha, id_empresa: 2, id_usuario: 1, id_conductor: 2, id_comuna: 2, id_tipo_estado: 3, created_at: fechaHora, updated_at: fechaHora});
    envios.push({monto : faker.random.number(), nombre_destinatario: faker.name.firstName(), rut: faker.random.number(), telefono: 4542112, direccion: faker.address.streetAddress(), numero: faker.random.number(), correo: faker.internet.email(), comentario: faker.lorem.words(), fecha_compra: fecha, fecha_entrega: fecha, id_empresa: 3, id_usuario: 1, id_conductor: 2, id_comuna: 3, id_tipo_estado: 2, created_at: fechaHora, updated_at: fechaHora});
   
    await queryInterface.bulkInsert('envios', envios, {});
  },

  down: async (queryInterface, Sequelize) => { 
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
