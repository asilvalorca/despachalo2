'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let zonas =[];
    let zonaComunas =[];
    const hoy = new Date();
    const fecha = hoy.getFullYear() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getDate();
    const hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
    const fechaHora = fecha+" "+hora;
    zonas.push({zona : "Norte", created_at: fechaHora, updated_at: fechaHora});
    zonas.push({zona : "Centro", created_at: fechaHora, updated_at: fechaHora});
    zonas.push({zona : "Sur", created_at: fechaHora, updated_at: fechaHora});
    await queryInterface.bulkInsert('zonas', zonas, {});
    /*
    for(let i = 0; i<= 3; i++){
      zonaComunas.push({ZonaIdZona : 1,ComunaIdComuna: i, created_at: fechaHora, updated_at: fechaHora});
    }
    for(let i = 0; i<= 3; i++){
      zonaComunas.push({ZonaIdZona : 2,ComunaIdComuna: i, created_at: fechaHora, updated_at: fechaHora});
    }
    for(let i = 0; i<= 3; i++){
      zonaComunas.push({ZonaIdZona : 3,ComunaIdComuna: i, created_at: fechaHora, updated_at: fechaHora});
    }
    */
    
    
    zonaComunas.push({zona_id_zona : 1,comuna_id_comuna: 2, created_at: fechaHora, updated_at: fechaHora});
    zonaComunas.push({zona_id_zona : 1,comuna_id_comuna: 3, created_at: fechaHora, updated_at: fechaHora});
    zonaComunas.push({zona_id_zona : 1,comuna_id_comuna: 4, created_at: fechaHora, updated_at: fechaHora});
    zonaComunas.push({zona_id_zona : 1,comuna_id_comuna: 5, created_at: fechaHora, updated_at: fechaHora});
    zonaComunas.push({zona_id_zona : 2,comuna_id_comuna: 1, created_at: fechaHora, updated_at: fechaHora});
    zonaComunas.push({zona_id_zona : 2,comuna_id_comuna: 2, created_at: fechaHora, updated_at: fechaHora});
    zonaComunas.push({zona_id_zona : 2,comuna_id_comuna: 3, created_at: fechaHora, updated_at: fechaHora});
    zonaComunas.push({zona_id_zona : 2,comuna_id_comuna: 4, created_at: fechaHora, updated_at: fechaHora});
    zonaComunas.push({zona_id_zona : 2,comuna_id_comuna: 5, created_at: fechaHora, updated_at: fechaHora});
    await queryInterface.bulkInsert('zona_comuna', zonaComunas, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
