'use strict';
const faker = require("faker");

module.exports = {
  up: async (queryInterface, Sequelize) => {

    let empresas =[];
    const hoy = new Date();
    const fecha = hoy.getFullYear() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getDate();
    const hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
    const fechaHora = fecha+" "+hora;

    for(let i = 0; i<= 10; i++){
      empresas.push({nombre : faker.company.companyName(),giro : faker.company.companySuffix(), direccion : faker.address.streetAddress(), id_comuna: 1, rut: faker.random.number(), created_at: fechaHora, updated_at: fechaHora})
    }
    await queryInterface.bulkInsert('empresas', empresas, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
