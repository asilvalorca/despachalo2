'use strict';
const faker = require("faker");
const bcrypt = require("bcrypt");


module.exports = {
  up: async (queryInterface, Sequelize) => {

    const saltRounds = 10;
    const clave = bcrypt.hashSync('1234', saltRounds);
    let usuarios =[];
    let usuariozona =[];
    const hoy = new Date();
    const fecha = hoy.getFullYear() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getDate();
    const hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
    const fechaHora = fecha+" "+hora;
    usuarios.push({username : "asilva", password: clave, nombre: faker.name.firstName(), apellido: faker.name.lastName(), correo: faker.internet.email(), estado: 1, id_empresa: null, id_perfil: 1, created_at: fechaHora, updated_at: fechaHora})
    usuarios.push({username : "lpoli", password: clave, nombre: faker.name.firstName(), apellido: faker.name.lastName(), correo: faker.internet.email(), estado: 1, id_empresa: null, id_perfil: 2, created_at: fechaHora, updated_at: fechaHora})
    usuarios.push({username : "lvoldemort",  password: clave, nombre: faker.name.firstName(), apellido: faker.name.lastName(), correo: faker.internet.email(), estado: 1, id_empresa: 1, id_perfil: 3, created_at: fechaHora, updated_at: fechaHora})
    
   
    await queryInterface.bulkInsert('usuarios', usuarios, {});
    usuariozona.push({usuario_id_usuario : 2, zona_id_zona: 1, created_at: fechaHora, updated_at: fechaHora})
    await queryInterface.bulkInsert('usuario_zona', usuariozona, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
