'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let tipoestados =[];
    const hoy = new Date();
    const fecha = hoy.getFullYear() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getDate();
    const hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
    const fechaHora = fecha+" "+hora;
    tipoestados.push({tipoestado : "Pendiente por Retiro", created_at: fechaHora, updated_at: fechaHora})
    tipoestados.push({tipoestado : "Retirado", created_at: fechaHora, updated_at: fechaHora})
    tipoestados.push({tipoestado : "Asignado", created_at: fechaHora, updated_at: fechaHora})
    tipoestados.push({tipoestado : "En Despacho", created_at: fechaHora, updated_at: fechaHora})
    tipoestados.push({tipoestado : "Recepcionado", created_at: fechaHora, updated_at: fechaHora})
    tipoestados.push({tipoestado : "Primer Intento", created_at: fechaHora, updated_at: fechaHora})
    tipoestados.push({tipoestado : "Segundo Intento", created_at: fechaHora, updated_at: fechaHora})
    tipoestados.push({tipoestado : "Devuelto", created_at: fechaHora, updated_at: fechaHora})
    tipoestados.push({tipoestado : "Cancelado", created_at: fechaHora, updated_at: fechaHora})

    await queryInterface.bulkInsert('tipo_estados', tipoestados, {}); 
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
