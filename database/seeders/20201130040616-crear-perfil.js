'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let perfiles =[];
    const hoy = new Date();
    const fecha = hoy.getFullYear() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getDate();
    const hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
    const fechaHora = fecha+" "+hora;
    perfiles.push({perfil : "Administrador", created_at: fechaHora, updated_at: fechaHora})
    perfiles.push({perfil : "Conductor", created_at: fechaHora, updated_at: fechaHora})
    perfiles.push({perfil : "Cliente", created_at: fechaHora, updated_at: fechaHora})
   
    await queryInterface.bulkInsert('perfils', perfiles, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
