import express from "express";
const router = express.Router();
import { check, validationResult } from "express-validator";
import upload from "./middlewares/upload";


import ComunaController from "../app/controllers/ComunaController";
import EmpresaController from "../app/controllers/EmpresaController";
import EnvioController from "../app/controllers/EnvioController";
import PerfilController from "../app/controllers/PerfilController";
import TipoEstadoController from "../app/controllers/TipoEstadoController";
import UsuarioController from "../app/controllers/UsuarioController";
import ZonaController from "../app/controllers/ZonaController";
import PagoController from "../app/controllers/PagoController";
/***************************************************** Comuna *********************************************/
//************************ Crear comuna *************************
router.post("/nueva-comuna",[
    check('comuna',"El nombre de la comuna es obligatorio").not().isEmpty() 
], ComunaController.crear);

//************************ Listar Comunas *************************
router.get("/listar-comunas", ComunaController.listarComunas);

//************************ Listar comunas Activas *************************
router.get("/listar-comunas-activas", ComunaController.listarComunasActivas);

//************************ Actualizar comunas *************************
router.put("/comuna/:id",[
    check('comuna',"El comuna  es obligatorio").not().isEmpty(),    
], ComunaController.actualizar);




/***************************************************** Empresa *********************************************/
//************************ Crear Empresa *************************
router.post("/nueva-empresa",[
    check('nombre',"El nombre de empresa es obligatorio").not().isEmpty(),
    check('giro',"El giro es obligatorio").not().isEmpty(),
    check('rut',"El rut es obligatorio").not().isEmpty(), 
   
], EmpresaController.crear);

//************************ Listar Empresas *************************
router.get("/listar-empresas", EmpresaController.listarEmpresas);

//************************ Listar Empresas Activas *************************
router.get("/listar-empresas-activas", EmpresaController.listarEmpresasActivas);

//************************ Actualizar Empresas *************************
router.put("/empresa/:id",[
    check('nombre',"El nombre de empresa es obligatorio").not().isEmpty(),
    check('giro',"El giro es obligatorio").not().isEmpty(),
    check('rut',"El rut es obligatorio").not().isEmpty(),   
], EmpresaController.actualizar);




/***************************************************** Perfil *********************************************/
//************************ Crear Perfil *************************
router.post("/nuevo-perfil",[
    check('perfil',"El perfil  es obligatorio").not().isEmpty()   
], PerfilController.crear);

//************************ Listar Perfiles *************************
router.get("/listar-perfiles", PerfilController.listarPerfiles);

//************************ Listar Perfiles Activas *************************
router.get("/listar-perfiles-activos", PerfilController.listarPerfilesActivos);

//************************ Actualizar Perfil *************************
router.put("/perfil/:id",[
    check('perfil',"El perfil  es obligatorio").not().isEmpty(), 
], PerfilController.actualizar);



/***************************************************** TipoEstado *********************************************/
//************************ Crear TipoEstado *************************
router.post("/nuevo-tipoestado",[
    check('tipoestado',"El tipoestado  es obligatorio").not().isEmpty()   
], TipoEstadoController.crear);

//************************ Listar TipoEstados *************************
router.get("/listar-tipoestados", TipoEstadoController.listarTipoEstado);

//************************ Listar Perfiles Activas *************************
router.get("/listar-tipoestados-activos", TipoEstadoController.listarTipoEstadoActivos);

//************************ Actualizar TipoEstados *************************
router.put("/tipoestado/:id",[
    check('tipoestado',"El tipoestado  es obligatorio").not().isEmpty(),
], TipoEstadoController.actualizar);



/***************************************************** Zona *********************************************/
//************************ Crear Zona *************************
router.post("/nueva-zona",[
    check('zona',"El nombre de la zona  es obligatorio").not().isEmpty()
   
], ZonaController.crear);

//************************ Listar Zonas *************************
router.get("/listar-zonas", ZonaController.listarZonas);

//************************ Listar Zonas Activas *************************
router.get("/listar-zonas-activos", ZonaController.listarZonasActivas);

//************************ Actualizar Zona *************************
router.put("/zona/:id",[
    check('zona',"El nombre de la zona es obligatoria").not().isEmpty(),
], ZonaController.actualizar);




/***************************************************** Usuario *********************************************/
//************************ Crear Usuario *************************
router.post("/nuevo-usuario",[
    check('username',"El nombre de usuario es obligatorio").not().isEmpty(),
    check('password',"El password  es obligatorio").not().isEmpty(),
    check('correo',"El mail  debe estar correcto").isEmail(),
    check('id_perfil',"El perfil  debe estar correcto").isInt(),
    check('nombre',"El nombre  es obligatorio").not().isEmpty(),
    check('apellido',"El apellido  es obligatorio").not().isEmpty(),
], UsuarioController.crear);

//************************ Listar Usuarios *************************
router.get("/listar-usuarios", UsuarioController.listarUsuarios);

//************************ Listar Usuarios Activas *************************
//router.get("/listar-zonas-activos", UsuarioController.listarUsuariosActivos);

//************************ Actualizar Usuario *************************
router.put("/usuario/:id",[
    check('username',"El nombre de usuario es obligatorio").not().isEmpty(),
    check('correo',"El mail  debe estar correcto").isEmail(),
    check('id_perfil',"El perfil  debe estar correcto").isInt(),
    check('nombre',"El nombre  es obligatorio").not().isEmpty(),
    check('apellido',"El apellido  es obligatorio").not().isEmpty(),
    check('estado',"El estado  es obligatorio").isInt(),
], UsuarioController.actualizar);

//************************ Login Usuarios *************************
router.post("/login", UsuarioController.login);

//************************ Login Usuarios *************************
router.get("/modificar", UsuarioController.modificar);

//************************ Listar conductores Activas *************************
router.get("/listar-conductores", UsuarioController.listarConductores);



/***************************************************** Envio *********************************************/
//************************ Crear Envios *************************
router.post("/nuevo-envio",[
    check('id_empresa',"La empresa es obligatoria").not().isEmpty(),
    check('id_comuna',"La comuna es obligatoria").not().isEmpty(),
    check('nombre_destinatario',"El nombre del destinario es obligatorio").not().isEmpty(),   
    check('telefono',"La teléfono es obligatoria").not().isEmpty(),
    check('correo',"El mail  debe estar correcto").isEmail(),    
    check('direccion',"La direccioón es obligatoria").not().isEmpty(),
  
], EnvioController.crear);

//************************ Consultar Envio *************************
router.get("/envio/:id", EnvioController.buscarEnvio);

//************************ Listar Envios *************************
router.get("/listar-envios", EnvioController.listarEnvios);

//************************ Listar Envios usuarios *************************
router.get("/listar-envios-usuario/:id", EnvioController.listarEnviosUsuario);

//************************ Actualizar Envios *************************
router.put("/envio/:id",[
   
    check('id_comuna',"La comuna es obligatoria").not().isEmpty(),
    check('nombre_destinatario',"El nombre del destinario es obligatorio").not().isEmpty(),   
    check('telefono',"La teléfono es obligatoria").not().isEmpty(),
    check('correo',"El mail  debe estar correcto").isEmail(),    
    check('direccion',"La direccioón es obligatoria").not().isEmpty(),
], EnvioController.actualizar);

//************************ Actualizar Estado Envio *************************
router.put("/envio-estado/:id", EnvioController.cambiarEstado);

//************************ Actualizar conductor *************************
router.put("/envio-conductor/:id", EnvioController.cambiarConductor);

//************************ Cargar Envios*************************
router.post("/cargar-envios/",upload.single('archivo'), EnvioController.cargarEnvio);

//************************ Carga de Trabajo Conductor*************************
router.get("/carga-conductor/:id", EnvioController.cargaConductor);
module.exports = router;

router.get("/correo", EnvioController.correo);
module.exports = router;


//************************  ********** Pagos ******** *************************
// ************ Listar pagos pendientes por empresa
router.get("/listar-pagos-pendientes/:id", PagoController.listarPagosPendientes);

// ************ Listar todos los pagos por empresa
router.get("/listar-pagos/:id", PagoController.listarPagos);

// ************ Listar todos los pagos pendientes
router.get("/listar-todos-pagos-pendientes/", PagoController.listarTodosPagosPendientes);

//************************ actualizar pago *************************
router.put("/hacer-pago/:id", PagoController.hacerPago);