const jwt = require('jsonwebtoken');

const verificarAuth = (req, res, next) => {

  const token = req.headers['token'];
console.log(token);


  jwt.verify(token, 'secret', (err, decoded) => {

    if(err){
      return res.status(401).json({
        mensaje: 'Usuario no válido',
        estado : 0,
        err 
      })
    }

    req.usuario = decoded.data;

    next();

  })

}

const verificarAdministrador = (req, res, next) => {

  const perfil = req.usuario.id_perfil

  if(perfil === 1){
    next();
  }else{
    return res.status(401).json({
      mensaje: 'Usuario no válido',
      estado: 0
    })
  }

}

module.exports = {verificarAuth, verificarAdministrador}