import multer from "multer"; 
import path from "path"; 


const storage = multer.diskStorage({
    destination: "./archivos",
    filename: (req, file, cb) =>{
        cb("",Date.now()+path.extname(file.originalname));
    }
});
const fileFilter = (req, file, cb) => {
    if (file.mimetype == 'application/vnd.ms-excel' || file.mimetype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

const upload = multer({
   storage,
   limits:{  fileSize: 1000000  },
   fileFilter

})

module.exports = upload;